<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/global.css">
    <link rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Accueil</title>
</head>
<body>
    <?php include("../navbar/navbar.php");?>
    <!--if error set-->
    <div class="row text-center m-3" style="width:300px; text-align:center">
        <?php
                if(isset($_GET['error'])){
                    switch ($_GET['error']){
                        case "not_registered":
                            echo '
                                <div class="alert alert-info fade in alert-dismissible show" >
                                    <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true" style="font-size:20px">x</span>
                                    </button>
                                    <strong>Mail/Tag did not match !</strong> Verify the mail or try to badge again.
                                </div>
                            ';
                        break;
                        case "dont_match":
                            echo '
                                <div class="alert alert-warning fade in alert-dismissible show">
                                    <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true" style="font-size:20px">x</span>
                                    </button>
                                    <strong>Password did not match !</strong>.
                                </div>
                            ';
                        break;               
                        
                        default: break;
                    }
                }
        ?>
    </div>


    <div class="container" style="max-width:500px;">
        <div class="row mb-3">
            <h3 class="text-center">Log in here with your credentials</h3><hr>
            <form action="../login/loginTraitment.php" method="post">
                <div class="mb-3">
                    <label for="mail" class="form-label">Email address</label>
                    <input type="email" name="mail" id="mail" placeholder="Mail: xxxx@domain.yyyy" class="form-control" aria-describedby="emailHelp">
                </div>
                <div class="mb-3"> 
                    <label for="pass" class="form-label">Password</label>
                    <input type="password" id="pass" name="pass" placeholder="Enter password" class="form-control" >
                </div>
                <input type="hidden" name="form_function" value="credentials">   
                <button type="submit" id="credentials" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="row mt-5">
            <hr>
			<a class="btn btn-primary" href="../scripts/connexion.php">Click here and apply badge</a>
            <!--<hr><h3 class="text-center">Or you can use your badge.</h3><br>
            <form class="text-center" action="../login/loginTraitment.php" method="post">
                <input type="hidden" name="form_function" value="badge">   
                <button type="submit" id="badge" class="btn btn-primary">Click here and apply badge</button>
            </form>-->
        </div>
    </div>    
</body>
</html>
