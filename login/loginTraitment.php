<?php
    session_start();//starting the session
    require_once("../bdd/config.inc.php");//connection to the database

    if(isset($_POST['form_function'])){
        //$connexionOk = false; 
        switch ($_POST['form_function']) {
            case "credentials":{
                echo "submitted credentials";
                if(isset($_POST['mail']) AND isset($_POST['pass'])){// verify if both mail and password is set before sending the form              
                    echo    'req';   
                    $req = $bdd->prepare('SELECT * FROM person WHERE mail = ?');
                    $req->execute(array($_POST['mail']));
                    $response = $req->fetch();
                    if(!$response){
                        echo    'no res';   
                        header('Location: ../login/login.php?error=not_registered');//no  email found in the database
                    }else{
                        if(password_verify($_POST['pass'], $response['pass'])){//only if the password matches
                            $_SESSION['connexionOk'] = "connexionOk";                                                   
                        }else{   
                            header('Location: ../login/login.php?error=dont_match');//Both password don't not match
                        }                    
                    }
                }
                break;
            }                    
            case "badge":{
				/*echo "submitted badge";
				unset($_POST['form_function']);
				header('Location: ../scripts/connexion.php');*/                
				break;
            }
            default:{
                header('Location: ../login/login.php');
                break;
            }
                
        }
    }
        if(isset($_SESSION['connexionOk']) && $_SESSION['connexionOk'] == "connexionOk"){
            echo 'ho';
			unset($_SESSION['connexionOk']);				
			if(isset($_SESSION['givenTag'] )){
				echo 'ha';
				$req = $bdd->prepare('SELECT * FROM person WHERE tag = ?');
				$req->execute(array($tag[0]));
				$response = $req->fetch();
			}
			$_SESSION['fullName'] = htmlspecialchars($response['fullName']);
            $_SESSION['userStatus'] = htmlspecialchars($response['userStatus']);
            $_SESSION['mail'] = htmlspecialchars($response['mail']);
            $_SESSION['hasVoted'] = htmlspecialchars($response['hasVoted']);
            $_SESSION['tag'] = htmlspecialchars($response['tag']); 
            $_SESSION['id'] = htmlspecialchars($response['id']); 
            echo    'con ok';
			unset($_SESSION['givenTag']);				
			
            if($response['userStatus']=="student"){
                header('Location: ../student/student.php');//go to the main student page
            }elseif($response['userStatus']=="professor"){
                header('Location: ../professor/professor.php');//go to the main professor page
            }else{
                header('Location: ../admin/admin.php');//go to the admin page
            }
        }else{
			header('Location: ../login/login.php?error=not_registered');//no  email/tag found in the database
		}

?>    