<?php 
    session_start();//starting the session
    require_once("../scripts/sessionCheck.php");
    if( !($_SESSION['userStatus'] == 'admin' ) ){
        header('Location: ../'.$_SESSION['userStatus'].'/'.$_SESSION['userStatus'].'.php');
    }else{?>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="#">
        <!--<img src="../home/esigLogo.jpg" alt="The logo of esigelec" style="width:40px;">-->
        <p>PING</p>
    </a>
    <!-- Links -->
    <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="../admin/admin.php">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="../admin/adminAddUser.php">Add User</a></li>
        <li class="nav-item"><a class="nav-link" href="../admin/adminAddPing.php">Add PING</a></li>
        <li class="nav-item"><a class="nav-link" href="../admin/adminVote.php">Vote</a></li>
        <li class="nav-item"><a class="nav-link" href="../admin/adminVisualizePing.php">Visualize PING</a></li>
        <li class="nav-item"><a class="nav-link" href="../admin/adminReboot.php">REBOOT</a></li>
        <li class="nav-item"><a class="nav-link" href="../login/logoff.php"> <?php echo $_SESSION['mail'].'';?> log off</a></li>
    </ul>
</nav>
<?php }?>