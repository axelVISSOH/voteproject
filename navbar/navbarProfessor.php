<?php 
    session_start();//starting the session
    require_once("../scripts/sessionCheck.php");
    if( !($_SESSION['userStatus'] == 'professor' ) ){
        header('Location: ../'.$_SESSION['userStatus'].'/'.$_SESSION['userStatus'].'.php');
    }else{?>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="#">
        <!--<img src="../home/esigLogo.jpg" alt="The logo of esigelec" style="width:40px;">-->
        <p>PING</p>
    </a>
    <!-- Links -->
    <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="../professor/professor.php">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="../professor/professorAddPing.php">Add Ping</a></li>
        <li class="nav-item"><a class="nav-link" href="../professor/professorVote.php">Vote</a></li>
        <?php if( !isset($_SESSION['mail'])){ ?>
            <li class="nav-item"><a class="nav-link" href="../login/login.php">log In</a></li>
        <?php }else{?>
            <li class="nav-item"><a class="nav-link" href="../login/logoff.php"> <?php echo $_SESSION['mail'].'';?> log off</a></li>
        <?php }?>
    </ul>
</nav>
<?php }?>