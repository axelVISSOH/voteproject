
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="#">
        <!--<img src="../home/esigLogo.jpg" alt="The logo of esigelec" style="width:40px;">-->
        <p>PING</p>
    </a>
    <!-- Links -->
    <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="../home/index.php">Home</a></li>
        <?php if( !isset($_SESSION['mail'])){ ?>
            <li class="nav-item"><a class="nav-link" href="../login/login.php">log In</a></li>
        <?php }else{?>
            <li class="nav-item"><a class="nav-link" href="../login/logoff.php"> <?php echo $_SESSION['mail'].'';?> log off</a></li>
        <?php }?>
    </ul>
</nav>