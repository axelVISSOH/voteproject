<?php
    session_start();//starting the session
    require_once("../bdd/config.inc.php");//connection to the database
    if( isset($_POST['name']) AND isset($_POST['description']) ){// verify if both mail and password is set before sending the form              
        if (isset($_FILES['poster']) AND $_FILES['poster']['error'] == 0){//test if the file is send & if there is no error
            // test if the file is voluminous
            if ($_FILES['poster']['size'] <= 1000000){
                // test if the extension is allowed
                $fileInfo = pathinfo($_FILES['poster']['name']);
                $extensionUpload = $fileInfo['extension'];
                $extensionsAllowed = array('png','jpeg','jpg');
                $timestamp = getdate(date("U"))['0'];
                if (in_array($extensionUpload, $extensionsAllowed)){
                    move_uploaded_file($_FILES['poster']['tmp_name'],'../pictures/posterPath/ping_'.basename(''.$timestamp.'_'.$_POST['name'].'_poster.jpg'));
                    $req = $bdd->prepare('INSERT INTO ping (name, description, poster, numberOfVotes) VALUES(?,?,?,?)');
                    $req->execute(array($_POST['name'], $_POST['description'], '/pictures/posterPath/ping_'.basename(''.$timestamp.'_'.$_POST['name'].'_poster.jpg') ,0));
                    header('Location: ../professor/professorAddPing.php?error=success');//if everything was good
                }
                else{
                    header('Location: ../professor/professorAddPing.php?error=extension');//if the extension is not good
                }
            }else{
                header('Location: ../professor/ProfessorAddPing.php?error=size');//if the size is too voluminous
            }
        }else{
            header('Location: ../professor/professorAddPing.php?error=error');//if the file is not sent or ther is a php error
        }
    }
    else{
        header('Location: ../professor/professorAddPing.php?error=missing');//some fiels are not set
    }
?>