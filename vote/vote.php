<?php 
    require_once("../bdd/config.inc.php");//connection to the database
    $req = $bdd->prepare('SELECT * FROM ping');
    $req -> execute();
?>
    <div class="container-fluid">
        <div class="row">
            <?php while( $response = $req->fetch() ){ 
                echo '
                    <div class="card m-3" style="width: 18rem;">
                        <h5 style="color:red" class="card-title m-1">'.htmlspecialchars($response['name']).'</h5><hr>
                        <p class="m-1" >'.htmlspecialchars($response['numberOfVotes']).' Vote(s) that project.</p>                                                      
                        <img style="max-width: 200px; max-height: 150px" src="..'.$response['poster'].'" class="card-img-top rounded mx-auto d-block" alt="...">
                        <div class="card-body">
                            <p class="card-text">'.htmlspecialchars($response['description']).'</p>
                            <hr>';?>

                            <?php 
                            if( $_SESSION['hasVoted'] == 0){
                                    echo '                    
                                    <a href="../vote/voteTraitement.php?pingId='.$response['id'].'" class="btn btn-primary"> Votez </a>
                                ';
                            }else{
                                echo '<p>Vous avez déjà voter.</p>                    
                                    <a href="#" class="btn btn-secondary"> Votez </a>
                                ';
                            }
                            echo '                    
                                    </div>
                                </div>
                                ';
            } ?>
        </div>
    </div>