<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/global.css">    
    <link rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Admin</title>
</head>
<body>
    <?php include("../navbar/navbarAdmin.php");?>

<!--if error set-->
<div class="row text-center m-3" style="width:300px; text-align:center">
        <?php
                if(isset($_GET['error'])){
                    switch ($_GET['error']){
                        case "missing":
                            echo '
                                <div class="alert alert-warning fade in alert-dismissible show" >
                                    <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true" style="font-size:20px">x</span>
                                    </button>
                                    <strong>Missing field.!</strong>You may fill all the fields.
                                </div>
                            ';
                        break;
                        case "pass":
                            echo '
                                <div class="alert alert-warning fade in alert-dismissible show">
                                    <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true" style="font-size:20px">x</span>
                                    </button>
                                    <strong>Password did not match !</strong>. pass and confirm pass fields are not the same.
                                </div>
                            ';                        
                        break;               
                        
                        default:
                            echo '
                                <div class="alert alert-danger fade in alert-dismissible show">
                                    <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true" style="font-size:20px">x</span>
                                    </button>
                                    <strong>User already exist</strong>.User with this mail/tag already exist with the name << '.htmlspecialchars($_GET['error']).' >>.
                                </div>
                            ';  
                        break;
                    }
                }
        ?>
    </div>

    <div class="text-center">
        <h3>Add an user</h3>        
    </div>
    <div class="container" style="max-width:500px;">
        <div class="row mb-3">
            <h3 class="text-center">Add user informations</h3><hr>
            <p>All fields are mandatories but you can add a user with no Badge.</p>
            <form action="../admin/adminAddUserTraitment.php" method="post">
                <div class="mb-3">
                    <label for="name" class="form-label">Full name</label>
                    <input type="text" name="name" id="name" placeholder="Jean DUPOND" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="mail" class="form-label">Email address</label>
                    <input type="email" name="mail" id="mail" placeholder="Mail: xxxx@domain.yyyy" class="form-control" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="pass" class="form-label">Password</label>
                    <input type="password" id="pass" name="pass" placeholder="Enter password" class="form-control" >
                </div>
                <div class="mb-3">
                    <label for="cpass" class="form-label">Confirm Password</label>
                    <input type="password" id="cpass" name="cpass" placeholder="Enter password" class="form-control" >
                </div>
                <div class="mb-3">
                    <p>
                        <label for="status">What's the user status?</label><br />
                        <select name="status" id="status">
                            <option value="student">Student</option>
                            <option value="professor">Professor</option>
                            <option value="admin">Admin</option>                           
                        </select>
                    </p>
                </div>
                <div class="mb-3">
                    <label  for="tag" class="form-label">Add a badge tag for this user.</label><br>
                    <input  type="radio" name="tag" value="no" id="no"  checked="checked" /> <label for="no">No</label>
                    <input  type="radio" name="tag" value="yes" id="yes" /> <label for="yes">Yes</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>    
</body>
</html>