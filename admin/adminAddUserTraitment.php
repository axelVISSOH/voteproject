<?php 
session_start();//starting the session
require_once("../bdd/config.inc.php");//connection to the database
    if( isset($_POST['name']) AND isset($_POST['mail']) AND isset($_POST['pass']) AND isset($_POST['cpass']) AND isset($_POST['status']) AND isset($_POST['tag'])){
        if($_POST['pass'] === $_POST['cpass']) {
            $pass_hache =  crypt($_POST['pass'], '$2a$10$1qAz2wSx3eDc4rFv5tGb5t');
            $req = $bdd->prepare('SELECT * FROM person WHERE mail = ?');
            $req->execute(array($_POST['mail']));
            $response = $req->fetch();
            print_r($response);
            if($response){
                echo    'user exists';   
                header('Location: ../admin/adminAddUser.php?error='.$response['fullName']);;//mail already exist
            }else{
                if( $_POST['tag'] === "no" ){
                    $req = $bdd->prepare('INSERT INTO person (fullName, mail, userStatus, pass, hasVoted) VALUES(?,?,?,?,?)');
                    $req->execute(array($_POST['name'], $_POST['mail'], $_POST['status'], $pass_hache, 0));
                    header('Location: ../admin/admin.php');
                }elseif( $_POST['tag'] === "yes" ){
                    header('Location: ../scripts/addUser.php?n='.$_POST['name'].'&m='.$_POST['mail'].'&p='.$_POST['pass'].'&s='.$_POST['status']);
                }
            }            
        }else{
           header('Location: ../admin/adminAddUser.php?error=pass');//pass & confirm pass not the same
        }
    }
    else{
        header('Location: ../admin/adminAddUser.php?error=missing');//some field are not set                
    }
?>