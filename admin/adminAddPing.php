<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/global.css">    
    <link rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Admin</title>
</head>
<body>
    <?php include("../navbar/navbarAdmin.php");?>
    
    <!--if error set-->
    <div class="row text-center m-3" style="width:300px; text-align:center">
        <?php
            if(isset($_GET['error'])){
                switch ($_GET['error']){
                    case 'success':
                        echo '
                            <div class="alert alert-success fade in alert-dismissible show">
                                <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" style="font-size:20px">x</span>
                                </button>
                                <strong>Poster added!</strong>.
                            </div>
                        ';
                    break;
                    case 'missing':
                        echo '
                            <div class="alert alert-warning fade in alert-dismissible show">
                                <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" style="font-size:20px">x</span>
                                </button>
                                <strong>Lack of information</strong> Please fill all the gaps.
                            </div>
                        ';
                    break;
                    case 'extension':
                        echo '
                            <div class="alert alert-danger fade in alert-dismissible show">
                                <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" style="font-size:20px">x</span>
                                </button>
                                <strong>Extension issue!</strong> Only JPG, JPEG, PNG are allowed.
                            </div>
                        ';
                    break;
                    case 'size':
                        echo '
                            <div class="alert alert-danger fade in alert-dismissible show">
                                <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" style="font-size:20px">x</span>
                                </button>
                                <strong>Size issue!</strong> picture should be maximum 1000000 Mb.
                            </div>
                        ';
                    break;
                    case 'error':
                        echo '
                            <div class="alert alert-danger fade in alert-dismissible show">
                                <button type="button" class="close"  data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" style="font-size:20px">x</span>
                                </button>
                                <strong>Error!!!</strong> There was an error please try again.
                            </div>
                        ';
                    break;
                    
                    default: break;
                }
            }
        ?>
    </div>
    
    <div class="text-center">
        <h3>Add a PING</h3>        
    </div>
    <div class="container" style="max-width:500px;">
        <div class="row mb-3">
            <h3 class="text-center">Add PING informations</h3><hr>
            <form action="../admin/adminAddPingTraitment.php" method="post" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="name" class="form-label">PING name</label>
                    <input type="text" name="name" id="name" placeholder="Virtual Intelligente House..." class="form-control" >
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">PING Description</label>
                    <textarea name="description" id="description" class="form-control" ></textarea>
                </div>
                <div class="mb-3">
                    <p>
                        <label for="poster" class="form-label">Enter the poster (PING picture: <strong>JPEG, JPG, PNG only</strong>.)</label>
                        <input type="file" id="poster" name="poster" placeholder="JPEG, JPG, PNG only"/><br />
                    </p>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>    
</body>
</html>