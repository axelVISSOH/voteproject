<?php 
    require_once("../bdd/config.inc.php");//connection to the database
    $req = $bdd->prepare('SELECT * FROM ping');
    $req -> execute();
    $pingAndVotes = [];
    $response = $req->fetchAll();
    for($i=0; $i<count($response); $i++){
      $pingAndVotes[$i]= array('name'=> htmlspecialchars($response[$i]['name']),'votes'=>$response[$i]['numberOfVotes']);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        var values = <?php echo json_encode($pingAndVotes); ?>;
        var pingAndVotes = new Array(values.length);
        for (var i = 0; i < pingAndVotes.length; i++){
          pingAndVotes[i] = new Array(2);
          pingAndVotes[i][0] = values[i]['name'];
          pingAndVotes[i][1] = parseInt(values[i]['votes']);
        }
        
        data.addRows(pingAndVotes);
        
        // Set chart options
        var options = {'title':'How are votes going (PING with 0 vote will not appear.)',
                       'width':800,
                       'height':400};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>

    <link rel="stylesheet" href="../styles/global.css">    
    <link rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Admin</title>
</head>
<body>
    <?php include("../navbar/navbarAdmin.php");?> 
    
    <div class="text-center">
        <h3>Visualize all the PING</h3>        
    </div>
    <div class="row m-5">
        <div class="col-4" style="height: 400px;overflow-y:scroll;">
          <table class="mb-3 table table-bordered table-dark table-striped table-hover">
              <thead>
                  <tr>
                      <th scope="col">PING Name</th>
                      <th scope="col">Number of Votes</th>
                  </tr>
              </thead>
              <tbody>
                <?php 
                  for($i=0; $i<count($pingAndVotes); $i++) { ?>
                    <tr>
                      <td><?php echo $pingAndVotes[$i]['name'] ?></td>
                      <td><?php echo $pingAndVotes[$i]['votes']?></td>                    
                    </tr>
                <?php } ?>
              </tbody>
          </table>
        </div>
        <!--Div that will hold the pie chart-->
        <div class="col-8">
          <div id="chart_div"></div>
        </div>
    </div>    
</body>
</html>
<!--
  style="margin-left: 20%;" -->
  