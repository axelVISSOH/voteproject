<?php 
    session_start();//starting the session
    require_once("../bdd/config.inc.php");//connection to the database
    if( isset($_SESSION['userStatus']) && ($_SESSION['userStatus'] == 'admin')){
        #delete ping in database
        $req = $bdd->prepare('DELETE FROM ping WHERE id>0');
        $req->execute();
        #delete psterPath
        #exec("rm ../posterPath/*jpg ../posterPath/*png  ../posterPath/*jpeg");
        include("../scripts/deletefolder.php");
        #set all users votes to 0
        $req = $bdd->prepare('UPDATE person SET hasVoted = 0 WHERE id>0');
        $req->execute();
        echo 'OK';
        header('Location: ../admin/admin.php');
    }else{
        header('Location: ../login/login.php');
    }    
?>