-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2022 at 04:20 PM
-- Server version: 5.6.20-log
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projetras`
--

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
`id` int(11) NOT NULL,
  `fullName` varchar(256) NOT NULL,
  `mail` varchar(256) NOT NULL,
  `pass` varchar(256) NOT NULL,
  `tag` varchar(256) DEFAULT NULL,
  `hasVoted` tinyint(1) NOT NULL,
  `userStatus` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `fullName`, `mail`, `pass`, `tag`, `hasVoted`, `userStatus`) VALUES
(1, 'toto', 'toto@admin.fr', 'toto', NULL, 0, 'admin'),
(2, 'KOUVIS', 'kouvis@admin.fr', '$2a$10$1qAz2wSx3eDc4rFv5tGb5eW3qxKjZQcH52JDwOoV2ZBmry3ePjNg2', 'd4a1d7de', 0, 'admin'),
(3, 'toto', 'toto2@toto.fr', '$2a$10$1qAz2wSx3eDc4rFv5tGb5e8S5i.OC2H4.5fP46bAoXDHC/BmzE.ty', 'sDGQRHQER', 0, 'student'),
(4, 'Vincent DERRIEN', 'vincent@esigelec.fr', '$2a$10$1qAz2wSx3eDc4rFv5tGb5el1rDnusFtDWJqY74mWf2VBlBlxPcWee', '', 0, 'professor');

-- --------------------------------------------------------

--
-- Table structure for table `ping`
--

CREATE TABLE IF NOT EXISTS `ping` (
`id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `poster` varchar(256) NOT NULL,
  `numberOfVotes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `person`
--
ALTER TABLE `person`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ping`
--
ALTER TABLE `ping`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ping`
--
ALTER TABLE `ping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
